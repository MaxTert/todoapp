package tertmax.example.todovalera;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class ToDoRecyclerViewAdapter extends RecyclerView.Adapter<ToDoRecyclerViewAdapter.ToDoViewHolder> {
    static final String REQUESTCODE = "requestcode";
    private static List<ToDoItem> mToDoItemList = new ArrayList<>();
    private Context mContext;

    ToDoRecyclerViewAdapter(Context context) {
        super();
        mContext = context;
    }

    static List<ToDoItem> getmToDoItemList() {
        return mToDoItemList;
    }

    void add(ToDoItem item) {
        mToDoItemList.add(0, item);
        makeSort(ToDoListActivity.getmSortData());
        notifyDataSetChanged();
        ((ToDoListActivity) mContext).refreshEmptyImageView();
    }

    void makeSort(int[] sortData) {
        SortingManager sortingManager = new SortingManager(sortData);
        sortingManager.makeSort();
        notifyDataSetChanged();
    }


    @Override
    public ToDoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        return new ToDoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ToDoViewHolder holder, final int position) {
        holder.status.setOnCheckedChangeListener(null);
        if (mToDoItemList.get(position).getStatus() == ToDoItem.Status.DONE) {
            holder.status.setChecked(true);
            holder.background.setImageResource(R.drawable.done_background);
        } else {
            holder.status.setChecked(false);
            holder.background.setImageResource(0);
        }
        holder.status.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mToDoItemList.get(position).setStatus(ToDoItem.Status.DONE);
                    holder.background.setImageResource(R.drawable.done_background);
                } else {
                    mToDoItemList.get(position).setStatus(ToDoItem.Status.INPROGRESS);
                    holder.background.setImageResource(0);
                }
            }
        });
        holder.title.setText(mToDoItemList.get(position).getTitle());
        holder.date.setText(mToDoItemList.get(position).getDate());
        holder.time.setText(mToDoItemList.get(position).getTime());
        holder.priorityImage.setImageResource(R.drawable.priority_med_icon);
        if (mToDoItemList.get(position).getPriority() == ToDoItem.Priority.HIGH) {
            holder.priorityImage.setImageResource(R.drawable.priority_high_icon);
        } else if (mToDoItemList.get(position).getPriority() == ToDoItem.Priority.LOW) {
            holder.priorityImage.setImageResource(R.drawable.priority_low_icon);
        }
    }


    @Override
    public int getItemCount() {
        return mToDoItemList.size();
    }

    private void removeAt(int position) {
        mToDoItemList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mToDoItemList.size());
    }

    void applyEditedItem(Intent data, String creationDate) {
        ToDoItem item = new ToDoItem(data);
        item.setCreationDate(creationDate);
        mToDoItemList.set(data.getIntExtra(ToDoItem.INDEX, 0), item);
        notifyDataSetChanged();
    }

    private void startEdit(int position) {
        Intent intent = ToDoItem.makeIntent(mToDoItemList.get(position).getTitle(),
                mToDoItemList.get(position).getDate(),
                mToDoItemList.get(position).getTime(),
                mToDoItemList.get(position).getPriority(),
                mToDoItemList.get(position).getStatus(),
                position);
        intent.putExtra(ToDoItem.CREATIONDATE, mToDoItemList.get(position).getStringCreationDate());
        intent.putExtra(REQUESTCODE, ToDoListActivity.EDIT_REQUEST_CODE);
        intent.setClass(mContext, AddNewToDoActivity.class);

        ((Activity) mContext).startActivityForResult(intent, ToDoListActivity.EDIT_REQUEST_CODE);
    }

    void removeAllItems() {
        mToDoItemList.clear();
        notifyDataSetChanged();
    }


    class ToDoViewHolder extends RecyclerView.ViewHolder implements
            View.OnLongClickListener,
            PopupMenu.OnMenuItemClickListener {
        CardView itemCardView;
        TextView title;
        TextView date;
        TextView time;
        CheckBox status;
        ImageView priorityImage;
        ImageView background;

        ToDoViewHolder(View itemView) {
            super(itemView);
            itemCardView = (CardView) itemView.findViewById(R.id.item_cardview);
            title = (TextView) itemView.findViewById(R.id.title);
            date = (TextView) itemView.findViewById(R.id.date);
            time = (TextView) itemView.findViewById(R.id.time);
            status = (CheckBox) itemView.findViewById(R.id.status_checkbox);
            priorityImage = (ImageView) itemView.findViewById(R.id.priority_imageview);
            background = (ImageView) itemView.findViewById(R.id.card_background_image);

            itemView.setOnLongClickListener(this);

        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.edit_popup: {
                    startEdit(getAdapterPosition());
                }
                break;
                case R.id.delete_popup: {
                    removeAt(getAdapterPosition());
                    ((ToDoListActivity) mContext).refreshEmptyImageView();
                }
                break;
                default:
                    return false;
            }
            return false;
        }


        @Override
        public boolean onLongClick(View v) {
            PopupMenu popup = new PopupMenu(v.getContext(), title);
            MenuInflater inflate = popup.getMenuInflater();
            inflate.inflate(R.menu.item_popupmenu, popup.getMenu());
            popup.setOnMenuItemClickListener(this);
            popup.show();

            return false;

        }

    }


}
