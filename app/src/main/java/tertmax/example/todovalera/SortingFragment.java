package tertmax.example.todovalera;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class SortingFragment extends Fragment {

    public static int NO_DATE_SORT = 0;
    public static int DESCEND_DATE_SORT = 1;
    public static int ASCEND_DATE_SORT = 2;
    public static int NO_PRIOR_SORT = 0;
    public static int DESCEND_PRIOR_SORT = 1;
    public static int ASCEND_PRIOR_SORT = 2;
    public static int NO_STATUS_SORT = 0;
    public static int DESCEND_STATUS_SORT = 1;
    public static int ASCEND_STATUS_SORT = 2;
    private static int[] data = new int[3];
    ImageButton ok_sorting_button;
    ImageButton cancel_sorting_buttton;
    ImageButton reset_sorting_button;

    RadioButton mNoDateRadioButton;
    RadioButton mDescDateRadioButton;
    RadioButton mAscDateRadioButton;

    RadioButton mNoPriorityRadioButton;
    RadioButton mDescPriorityRadioButton;
    RadioButton mAscPriorityRadioButton;

    RadioButton mNoStatusRadioButton;
    RadioButton mDescStatusRadioButton;
    RadioButton mAscStatusRadioButton;

    RadioButton mDefStatusRadioButton;
    RadioButton mDefPriorityRadioButton;
    RadioButton mDefDateRadioButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sorts_fragment, container, false);
        ok_sorting_button = (ImageButton) rootView.findViewById(R.id.ok_sorting_button);
        cancel_sorting_buttton = (ImageButton) rootView.findViewById(R.id.cancel_sorting_button);
        reset_sorting_button = (ImageButton) rootView.findViewById(R.id.reset_sorting_button);

        mNoDateRadioButton = (RadioButton) rootView.findViewById(R.id.no_datesort_radiobutton);
        mDescDateRadioButton = (RadioButton) rootView.findViewById(R.id.descending_datesort_radiobutton);
        mAscDateRadioButton = (RadioButton) rootView.findViewById(R.id.ascending_datesort_radiobutton);

        mNoStatusRadioButton = (RadioButton) rootView.findViewById(R.id.no_statussort_radiobutton);
        mDescStatusRadioButton = (RadioButton) rootView.findViewById(R.id.descending_statussort_radiobutton);
        mAscStatusRadioButton = (RadioButton) rootView.findViewById(R.id.ascending_statussort_radiobutton);

        mNoPriorityRadioButton = (RadioButton) rootView.findViewById(R.id.no_prioritysort_radiobutton);
        mDescPriorityRadioButton = (RadioButton) rootView.findViewById(R.id.descending_prioritysort_radiobutton);
        mAscPriorityRadioButton = (RadioButton) rootView.findViewById(R.id.ascending_prioritysort_radiobutton);

        if ((ToDoListActivity.getmSortData()[0] != 0) &&
                (ToDoListActivity.getmSortData()[1] != 0) &&
                (ToDoListActivity.getmSortData()[2] != 0))
            data = ToDoListActivity.getmSortData();

        if (data[0] == NO_DATE_SORT) {
            mNoDateRadioButton.setChecked(true);
            mDefDateRadioButton = mNoDateRadioButton;
        } else if (data[0] == DESCEND_DATE_SORT) {
            mDescDateRadioButton.setChecked(true);
            mDefDateRadioButton = mDescDateRadioButton;
        } else {
            mAscDateRadioButton.setChecked(true);
            mDefDateRadioButton = mAscDateRadioButton;
        }

        if (data[1] == NO_STATUS_SORT) {
            mNoStatusRadioButton.setChecked(true);
            mDefStatusRadioButton = mNoStatusRadioButton;
        } else if (data[1] == DESCEND_STATUS_SORT) {
            mDescStatusRadioButton.setChecked(true);
            mDefStatusRadioButton = mDescStatusRadioButton;
        } else {
            mAscStatusRadioButton.setChecked(true);
            mDefStatusRadioButton = mAscStatusRadioButton;
        }

        if (data[2] == NO_PRIOR_SORT) {
            mNoPriorityRadioButton.setChecked(true);
            mDefPriorityRadioButton = mNoPriorityRadioButton;
        } else if (data[2] == DESCEND_PRIOR_SORT) {
            mDescPriorityRadioButton.setChecked(true);
            mDefPriorityRadioButton = mDescPriorityRadioButton;
        } else {
            mAscPriorityRadioButton.setChecked(true);
            mDefPriorityRadioButton = mAscPriorityRadioButton;
        }

        RadioGroup dateRadioGroup = (RadioGroup) rootView.findViewById(R.id.datesort_radiogroup);
        RadioGroup statusRadioGroup = (RadioGroup) rootView.findViewById(R.id.statussort_radiogroup);
        RadioGroup priorityRadioGroup = (RadioGroup) rootView.findViewById(R.id.prioritysort_radiogroup);

        dateRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.no_datesort_radiobutton:
                        data[0] = NO_DATE_SORT;
                        break;
                    case R.id.descending_datesort_radiobutton:
                        data[0] = DESCEND_DATE_SORT;
                        break;
                    case R.id.ascending_datesort_radiobutton:
                        data[0] = ASCEND_DATE_SORT;
                        break;
                }
            }
        });
        statusRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.no_statussort_radiobutton:
                        data[1] = NO_STATUS_SORT;
                        break;
                    case R.id.descending_statussort_radiobutton:
                        data[1] = DESCEND_STATUS_SORT;
                        break;
                    case R.id.ascending_statussort_radiobutton:
                        data[1] = ASCEND_DATE_SORT;
                        break;
                }
            }
        });
        priorityRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.no_prioritysort_radiobutton:
                        data[2] = NO_PRIOR_SORT;
                        break;
                    case R.id.descending_prioritysort_radiobutton:
                        data[2] = DESCEND_PRIOR_SORT;
                        break;
                    case R.id.ascending_prioritysort_radiobutton:
                        data[2] = ASCEND_PRIOR_SORT;
                        break;
                }
            }
        });

        ok_sorting_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ToDoListActivity) getActivity()).setSort(data);
            }
        });
        reset_sorting_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSort();
            }
        });
        cancel_sorting_buttton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDefaultSort();
                ((ToDoListActivity) getActivity()).onCancel();
            }
        });

        return rootView;
    }

    public void setDefaultSort() {
        mDefDateRadioButton.setChecked(true);
        mDefStatusRadioButton.setChecked(true);
        mDefPriorityRadioButton.setChecked(true);
    }

    private void resetSort() {
        mNoDateRadioButton.setChecked(true);
        mNoStatusRadioButton.setChecked(true);
        mNoPriorityRadioButton.setChecked(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity;
        activity = (Activity) context;
        OnCancelClick mOnCancelClick;
        OnSortSet mOnSortSet;
        try {

            mOnSortSet = (OnSortSet) activity;
            mOnCancelClick = (OnCancelClick) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement interface");
        }
    }

    public interface OnSortSet {

        void setSort(int[] data);

    }

    public interface OnCancelClick {
        void onCancel();
    }

}
