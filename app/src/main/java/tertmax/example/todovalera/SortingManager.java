package tertmax.example.todovalera;

import java.util.Collections;

class SortingManager {

    private int[] mSortData = new int[3];

    SortingManager(int[] sortData){
        mSortData = sortData;
    }

    void makeSort(){

        if ((mSortData[0] == SortingFragment.NO_DATE_SORT) ||
                (mSortData[1] == SortingFragment.NO_STATUS_SORT) ||
                (mSortData[2] == SortingFragment.NO_PRIOR_SORT)) {
            recentSort();
        }

        if (mSortData[2] == SortingFragment.DESCEND_PRIOR_SORT) {
            prioritySort();
        }
        if (mSortData[2] == SortingFragment.ASCEND_PRIOR_SORT) {
            reversePrioritySort();
        }

        if (mSortData[0] == SortingFragment.DESCEND_DATE_SORT) {
            dateSort();
        }
        if (mSortData[0] == SortingFragment.ASCEND_DATE_SORT) {
            reverseDateSort();
        }
        if (mSortData[1] == SortingFragment.DESCEND_STATUS_SORT) {
            statusSort();
        }
        if (mSortData[1] == SortingFragment.ASCEND_STATUS_SORT) {
            reverseStatusSort();
        }

    }

    private void prioritySort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.PriorityComparator());
    }
    private void reversePrioritySort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.RevesrsePriorityComparator());
    }
    private void statusSort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.StatusComparator());
    }
    private void reverseStatusSort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.RevesrseStatusComparator());
    }
    private void dateSort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.DateComparator());
    }
    private void reverseDateSort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.ReverseDateComparator());
    }
    private void recentSort() {
        Collections.sort(ToDoRecyclerViewAdapter.getmToDoItemList(),
                new ToDoItem.RecentComparator());
    }
}
