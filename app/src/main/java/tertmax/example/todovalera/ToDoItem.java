package tertmax.example.todovalera;

import android.content.Intent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

class ToDoItem {

    private static final String ITEM_SEP = System.getProperty("line.separator");
    private final static DateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyyhh:mm", Locale.ENGLISH);
    final static String TITLE = "title";
    final static String DATE = "date";
    final static String TIME = "time";
    final static String PRIORITY = "priority";
    final static String STATUS = "status";
    final static String CREATIONDATE = "creationdate";
    static final String INDEX = "index";

    private String mTitle;
    private String mStringCreationDate;
    private Date mCreationDate;
    private Priority mPriority = Priority.MEDIUM;
    private Status mStatus = Status.INPROGRESS;
    private String mDate;
    private String mTime;

    ToDoItem(Intent intent) {

        mCreationDate = new Date();
        mTitle = intent.getStringExtra(TITLE);
        mDate = intent.getStringExtra(DATE);
        mTime = intent.getStringExtra(TIME);
        mPriority = Priority.valueOf(intent.getStringExtra(PRIORITY));
        mStatus = Status.valueOf(intent.getStringExtra(STATUS));

    }

    ToDoItem(String stringCreationDate, String title, String date, String time,
             Priority priority, Status status) {
        try {
            mCreationDate = FORMAT.parse(stringCreationDate);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        mTitle = title;
        mDate = date;
        mTime = time;
        mPriority = priority;
        mStatus = status;
    }
    String getStringCreationDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(mCreationDate);

        mStringCreationDate = AddNewToDoActivity.getStringDate(c.get(Calendar.YEAR),
                c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)) +
                AddNewToDoActivity.getStringTime(c.get(Calendar.HOUR_OF_DAY),
                        c.get(Calendar.MINUTE));
        return mStringCreationDate;
    }
    static Intent makeIntent(String title, String date, String time, Priority priority,
                             Status status, int index) {
        Intent intent = new Intent();
        intent.putExtra(TITLE, title);
        intent.putExtra(DATE, date);
        intent.putExtra(TIME, time);
        intent.putExtra(PRIORITY, priority.toString());
        intent.putExtra(STATUS, status.toString());
        intent.putExtra(INDEX, index);
        return intent;
    }

    public String toString() {

        return getStringCreationDate()+ ITEM_SEP + mTitle + ITEM_SEP
                + mDate + ITEM_SEP + mTime + ITEM_SEP + mPriority + ITEM_SEP + mStatus;
    }

    public String getTitle() {
        return mTitle;
    }

    Priority getPriority() {
        return mPriority;
    }

    Status getStatus() {
        return mStatus;
    }

    void setStatus(Status status) {
        mStatus = status;
    }

    String getDate() {
        return mDate;
    }

    public String getTime() {
        return mTime;
    }

    public void setCreationDate(String stringCreationDate) {
        mStringCreationDate = stringCreationDate;
        try {
            mCreationDate = FORMAT.parse(stringCreationDate);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
    }

    enum Priority {
        HIGH, MEDIUM, LOW
    }

    enum Status {
        DONE, INPROGRESS
    }

    static class PriorityComparator implements Comparator<ToDoItem> {

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            return o1.getPriority().compareTo(o2.getPriority());

        }
    }

    static class RevesrsePriorityComparator implements Comparator<ToDoItem> {

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            return -o1.getPriority().compareTo(o2.getPriority());
        }
    }

    static class StatusComparator implements Comparator<ToDoItem> {

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            return o1.getStatus().compareTo(o2.getStatus());

        }
    }

    static class RevesrseStatusComparator implements Comparator<ToDoItem> {

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            return -(o1.getStatus().compareTo(o2.getStatus()));
        }
    }

    static class DateComparator implements Comparator<ToDoItem> {
        Date dateOne;
        Date dateTwo;

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            try {
                dateOne = FORMAT.parse(o1.getDate() + o1.getTime());
                dateTwo = FORMAT.parse(o2.getDate() + o2.getTime());
                return -(dateOne.compareTo(dateTwo));
            } catch (ParseException pe) {
                pe.printStackTrace();
            }
            return 0;
        }
    }

    static class ReverseDateComparator implements Comparator<ToDoItem> {
        Date dateOne;
        Date dateTwo;

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            try {
                dateOne = FORMAT.parse(o1.getDate() + o1.getTime());
                dateTwo = FORMAT.parse(o2.getDate() + o2.getTime());
                return dateOne.compareTo(dateTwo);
            } catch (ParseException pe) {
                pe.printStackTrace();
            }
            return 0;
        }
    }


    static class RecentComparator implements Comparator<ToDoItem> {

        @Override
        public int compare(ToDoItem o1, ToDoItem o2) {
            return -o1.mCreationDate.compareTo(o2.mCreationDate);
        }
    }

}
