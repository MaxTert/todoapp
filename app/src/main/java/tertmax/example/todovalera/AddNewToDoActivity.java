package tertmax.example.todovalera;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

public class AddNewToDoActivity extends Activity {

    private static TextView mDateTextView;
    private static TextView mTimeTextView;
    private int itemIndex;
    private Intent editIntent;
    private Date mDate;
    private EditText mTitleEditText;
    private RadioGroup mPriorityRadioGroup;
    private RadioButton mDefaultPriorityRadioButton;
    private RadioButton mHighPriorityRadioButton;
    private RadioButton mLowPriorityRadioButton;


    static String getStringDate(int year, int month, int day) {
        month++;
        String y = "" + year;
        String m = "" + month;
        String d = "" + day;
        if (day < 10) {
            d = "0" + day;
        }
        if (month < 10) {
            m = "0" + month;
        }
        return d + "-" + m + "-" + y;
    }

    static String getStringTime(int hour, int minute) {
        String h = "" + hour;
        String m = "" + minute;
        if (hour < 10)
            h = "0" + hour;
        if (minute < 10)
            m = "0" + minute;
        return h + ":" + m;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_to_do);


        mTitleEditText = (EditText) findViewById(R.id.set_title_edittext);
        mPriorityRadioGroup = (RadioGroup) findViewById(R.id.priority_radiogroup);
        mDefaultPriorityRadioButton = (RadioButton) findViewById(R.id.priority_medium_radiobutton);
        mHighPriorityRadioButton = (RadioButton) findViewById(R.id.priority_high_radiobutton);
        mLowPriorityRadioButton = (RadioButton) findViewById(R.id.priority_low_radiobutton);
        mDateTextView = (TextView) findViewById(R.id.set_date_textview);
        mTimeTextView = (TextView) findViewById(R.id.set_time_textview);

        ImageButton cancelButton = (ImageButton) findViewById(R.id.cancel_button);
        ImageButton resetButton = (ImageButton) findViewById(R.id.reset_button);
        ImageButton okButton = (ImageButton) findViewById(R.id.ok_button);
        editIntent = getIntent();
        mTitleEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                    // NOTE: In the author's example, he uses an identifier
                    // called searchBar. If setting this code on your EditText
                    // then use v.getWindowToken() as a reference to your
                    // EditText is passed into this callback as a TextView

                    in.hideSoftInputFromWindow(v.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    // Must return true here to consume event
                    return true;

                }
                return false;
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) == 0)
                    setDefaultToDoItem();
                else if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) == 1) {
                    setEditToDoItem();
                }
            }
        });

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = ((EditText) findViewById(R.id.set_title_edittext))
                        .getText().toString();
                ToDoItem.Status status = ToDoItem.Status.INPROGRESS;
                if (!title.isEmpty()) {


                    String date = ((TextView) findViewById(R.id.set_date_textview))
                            .getText().toString();
                    String time = ((TextView) findViewById(R.id.set_time_textview))
                            .getText().toString();
                    if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) ==
                            ToDoListActivity.ADD_TODO_REQUEST_CODE) {
                        status = ToDoItem.Status.INPROGRESS;
                    } else if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) ==
                            ToDoListActivity.EDIT_REQUEST_CODE) {
                        if (editIntent.getStringExtra(ToDoItem.STATUS).equals(ToDoItem.Status.DONE.toString()))
                            status = ToDoItem.Status.DONE;
                        else
                            status = ToDoItem.Status.INPROGRESS;
                    }
                    ToDoItem.Priority priority = getPriority();

                    Intent intent = ToDoItem.makeIntent(title, date, time, priority, status, itemIndex);
                    if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) ==
                            ToDoListActivity.EDIT_REQUEST_CODE)
                        intent.putExtra(ToDoItem.CREATIONDATE,
                                editIntent.getStringExtra(ToDoItem.CREATIONDATE));
                    setResult(RESULT_OK, intent);
                    finish();
                } else Toast.makeText(getApplicationContext(), "You should enter the title",
                        Toast.LENGTH_SHORT).show();
            }
        });

        mDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        });

        mTimeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        });

        if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) == 0) {
            setDefaultToDoItem();
        } else if (editIntent.getIntExtra(ToDoRecyclerViewAdapter.REQUESTCODE, 0) == 1) {
            setEditToDoItem();
        }


    }

    private void setEditToDoItem() {
        mDateTextView.setText(editIntent.getStringExtra(ToDoItem.DATE));
        mTimeTextView.setText(editIntent.getStringExtra(ToDoItem.TIME));
        mTitleEditText.setText(editIntent.getStringExtra(ToDoItem.TITLE));
        itemIndex = editIntent.getIntExtra(ToDoItem.INDEX, 0);
        if (editIntent.
                getStringExtra(ToDoItem.PRIORITY).equals(ToDoItem.Priority.HIGH.toString())) {
            mHighPriorityRadioButton.setChecked(true);
        } else if (editIntent.
                getStringExtra(ToDoItem.PRIORITY).equals(ToDoItem.Priority.LOW.toString())) {
            mLowPriorityRadioButton.setChecked(true);
        } else
            mDefaultPriorityRadioButton.setChecked(true);
    }

    private void setDefaultDateAndTime() {
        mDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mDate);
        mDateTextView.setText(getStringDate(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        mTimeTextView.setText(getStringTime(calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE)));
    }

    private void setDefaultToDoItem() {
        mTitleEditText.setText("");
        mDefaultPriorityRadioButton.setChecked(true);
        setDefaultDateAndTime();

    }

    private ToDoItem.Priority getPriority() {
        switch (mPriorityRadioGroup.getCheckedRadioButtonId()) {
            case R.id.priority_low_radiobutton: {
                return ToDoItem.Priority.LOW;
            }
            case R.id.priority_high_radiobutton: {
                return ToDoItem.Priority.HIGH;
            }
            default: {
                return ToDoItem.Priority.MEDIUM;
            }
        }
    }

    private void showTimeDialog() {
        DialogFragment timeFragment = new TimePickerFragment();
        timeFragment.show(getFragmentManager(), "timeFragment");
    }

    private void showDateDialog() {
        DialogFragment dateFragment = new DatePickerFragment();
        dateFragment.show(getFragmentManager(), "dateFragment");
    }

    public static class DatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return datePickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mDateTextView.setText(getStringDate(year, month, dayOfMonth));
        }
    }

    public static class TimePickerFragment extends DialogFragment implements
            TimePickerDialog.OnTimeSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            return new TimePickerDialog(getActivity(), this, hour, minute, true);
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            mTimeTextView.setText(getStringTime(hourOfDay, minute));
        }
    }

}
