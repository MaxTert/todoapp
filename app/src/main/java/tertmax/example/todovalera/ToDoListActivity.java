package tertmax.example.todovalera;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


public class ToDoListActivity extends AppCompatActivity implements
        SortingFragment.OnSortSet,
        SortingFragment.OnCancelClick {

    public static final int ADD_TODO_REQUEST_CODE = 0;
    public static final int EDIT_REQUEST_CODE = 1;
    private static final String FILE_NAME = "ToDoList.txt";
    private static final String FIRST_SORT_CONF = "first_sort_conf";
    private static final String SECOND_SORT_CONF = "second_sort_conf";
    private static final String THIRD_SORT_CONF = "third_sort_conf";
    private static int[] mSortData = new int[3];
    FragmentTransaction fragmentTransaction;
    SortingFragment fragment;
    SharedPreferences sPref;
    private ImageView emptyListImageView;
    private ImageView mShadowImageView;
    private FloatingActionButton fab;
    private String deleteAlettMessage;
    private ToDoRecyclerViewAdapter mAdapter;
    private Context mContext;
    private String alertDialogOk;
    private String alertDialogCancel;

    public static int[] getmSortData() {
        return mSortData;
    }

    void refreshEmptyImageView() {
        if (ToDoRecyclerViewAdapter.getmToDoItemList().isEmpty()) {
            emptyListImageView.setVisibility(View.VISIBLE);
        } else
            emptyListImageView.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        emptyListImageView = (ImageView) findViewById(R.id.empty_list_imageview);
        mContext = this;
        mAdapter = new ToDoRecyclerViewAdapter(mContext);
        deleteAlettMessage = getString(R.string.deleteAllMessage);
        alertDialogOk = getString(R.string.alertdialog_ok);
        alertDialogCancel = getString(R.string.alertdialog_cancel);
        mShadowImageView = (ImageView) findViewById(R.id.shadow_imageview);
        mShadowImageView.setVisibility(View.INVISIBLE);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new FabOnClickListener());
        RecyclerView todoRecyclerView = (RecyclerView) findViewById(R.id.todo_recyclerview);
        todoRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        todoRecyclerView.setLayoutManager(layoutManager);
        todoRecyclerView.setAdapter(mAdapter);


    }

    @Override
    public void onBackPressed() {

        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
        } else {
            ((SortingFragment) getFragmentManager().findFragmentById(R.id.fragment_container))
                    .setDefaultSort();
            getFragmentManager().popBackStack();
            mShadowImageView.setVisibility(View.INVISIBLE);
        }

    }

    void startSortingFragment() {

        mShadowImageView.setVisibility(View.VISIBLE);
        fab.hide();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragment = new SortingFragment();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if ((requestCode == ADD_TODO_REQUEST_CODE) && (resultCode == RESULT_OK)) {
            mAdapter.add(new ToDoItem(data));
        }
        if ((requestCode == EDIT_REQUEST_CODE) && (resultCode == RESULT_OK)) {
            mAdapter.applyEditedItem(data, data.getStringExtra(ToDoItem.CREATIONDATE));
        }
        if (resultCode == RESULT_CANCELED) {
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_to_do_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_sort) {
            startSortingFragment();
            return true;
        }
        if (id == R.id.action_delete_all) {
            showDialog(this);

        }
        if (id == R.id.action_refresh) {
            setSort(mSortData);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveItems();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ToDoRecyclerViewAdapter.getmToDoItemList().size() == 0)
            loadItems();

    }

    @Override
    public void setSort(int[] data) {
        mSortData = data;
        mAdapter.makeSort(mSortData);
        removeFragment();
    }

    private void removeFragment() {
        mShadowImageView.setVisibility(View.INVISIBLE);
        fab.show();

        FragmentManager fragmentManager = getFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();
        if(fragmentManager.findFragmentById(R.id.fragment_container) != null)
        fragmentTransaction.remove(fragmentManager.findFragmentById(R.id.fragment_container));
        fragmentTransaction.commit();
    }

    @Override
    public void onCancel() {

        removeFragment();
    }

    private void removeAllItems() {

        mAdapter.removeAllItems();
        removeFragment();
        refreshEmptyImageView();
        fab.show();
    }

    private void loadItems() {
        sPref = getPreferences(MODE_PRIVATE);
        mSortData[0] = sPref.getInt(FIRST_SORT_CONF, 0);
        mSortData[1] = sPref.getInt(SECOND_SORT_CONF, 0);
        mSortData[2] = sPref.getInt(THIRD_SORT_CONF, 0);
        BufferedReader reader = null;
        try {
            FileInputStream fis = openFileInput(FILE_NAME);
            reader = new BufferedReader(new InputStreamReader(fis));

            String creationDate;
            String title;
            String date;
            String time;
            String priority;
            String status;

            while (null != (creationDate = reader.readLine())) {
                title = reader.readLine();
                date = reader.readLine();
                time = reader.readLine();
                priority = reader.readLine();
                status = reader.readLine();
                mAdapter.add(new ToDoItem(creationDate, title, date, time, ToDoItem.Priority.valueOf(priority),
                        ToDoItem.Status.valueOf(status)));
            }


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void showDialog(Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setMessage(deleteAlettMessage);
        builder.setPositiveButton(alertDialogOk, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeAllItems();
            }
        });
        builder.setNegativeButton(alertDialogCancel, null);
        builder.show();
    }

    private void saveItems() {
        PrintWriter writer = null;
        sPref = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putInt(FIRST_SORT_CONF, mSortData[0]);
        ed.putInt(SECOND_SORT_CONF, mSortData[1]);
        ed.putInt(THIRD_SORT_CONF, mSortData[2]);
        ed.apply();
        try {
            FileOutputStream fos = openFileOutput(FILE_NAME, MODE_PRIVATE);
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    fos)));
            for (int idx = 0; idx < ToDoRecyclerViewAdapter.getmToDoItemList().size(); idx++) {
                writer.println(ToDoRecyclerViewAdapter.getmToDoItemList().get(idx));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != writer) {
                writer.close();
            }
        }
    }

    class FabOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            Intent intent = new Intent(mContext, AddNewToDoActivity.class);
            startActivityForResult(intent, ADD_TODO_REQUEST_CODE);

        }
    }
}
